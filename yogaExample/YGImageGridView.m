//
//  YGImageGridView.m
//  yogaExample
//
//  Created by vvveiii on 2018/7/13.
//  Copyright © 2018年 vvveiii. All rights reserved.
//

#import "YGImageGridView.h"
#import <yogaKit/UIView+Yoga.h>
#import "UIImage+color.h"

@interface YGImageGridView () {
    NSMutableArray *_imageViewArray;
}

@property(nonatomic,strong) UIView *contentView;

@end

@implementation YGImageGridView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
        }];

        _contentView = [[UIView alloc] initWithFrame:CGRectZero];
        [_contentView configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.flexGrow = 1;
            layout.flexWrap = YGWrapWrap;
            layout.flexDirection = YGFlexDirectionRow;
            layout.justifyContent = YGJustifySpaceBetween;
            layout.alignContent = YGAlignSpaceBetween;
        }];
        [self addSubview:_contentView];

        _imageViewArray = [NSMutableArray array];
        for (NSInteger i = 0; i < 9; i++) {
            UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithColor:[UIColor orangeColor]]];
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
            [self.contentView addSubview:imageView];
            [_imageViewArray addObject:imageView];
        }
    }

    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    NSInteger col = 3, space = 2;
    CGFloat w = ceilf((CGRectGetWidth(self.bounds) - space * (col - 1)) / col);

    for (UIImageView *imageView in _imageViewArray) {
        [imageView configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.width = YGPointValue(w);
            layout.height = YGPointValue(w);
        }];
    }

    [self.contentView.yoga applyLayoutPreservingOrigin:YES];
}

@end
